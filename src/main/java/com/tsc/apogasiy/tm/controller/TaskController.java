package main.java.com.tsc.apogasiy.tm.controller;

import main.java.com.tsc.apogasiy.tm.api.controller.ITaskController;
import main.java.com.tsc.apogasiy.tm.api.service.ITaskService;
import main.java.com.tsc.apogasiy.tm.enumerated.Sort;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyTaskList;
import main.java.com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Task;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        // Завершаем, если нет задач
        if (taskService.isEmpty())
            throw new EmptyTaskList();

        final List<Task> tasks;
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();

        if (sort == null || sort.isEmpty() || !Sort.isValid(sort))
            tasks = taskService.findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            tasks = taskService.findAll(sortType.getComparator());
        }

        System.out.println("[LIST TASKS]");

        for (Task task : tasks)
            System.out.println(task);
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showTask(Task task) {
        if (task == null)
            return;
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
    }

    @Override
    public void showById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(id);
        if (task == null)
            throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findByName(name);
        if (task == null)
            throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null)
            throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void updateByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!taskService.existsByIndex(index))
            throw new TaskNotFoundException();
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.updateByIndex(index, name, description);
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        if (!taskService.existsById(id))
            throw new TaskNotFoundException();
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.updateById(id, name, description);
        System.out.println("[OK]");
    }

    @Override
    public void removeById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        if (!taskService.existsById(id))
            throw new TaskNotFoundException();
        taskService.removeById(id);
        System.out.println("[OK]");
    }

    @Override
    public void removeByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        if (!taskService.existsByName(name))
            throw new TaskNotFoundException();
        taskService.removeByName(name);
        System.out.println("[OK]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!taskService.existsByIndex(index))
            throw new TaskNotFoundException();
        taskService.removeByIndex(index);
        System.out.println("[OK]");
    }

    @Override
    public void startById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startById(id);
        if (task == null)
            throw new TaskNotFoundException();
    }

    @Override
    public void startByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startByIndex(index);
        if (task == null)
            throw new TaskNotFoundException();
    }

    @Override
    public void startByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startByName(name);
        if (task == null)
            throw new TaskNotFoundException();
    }

    @Override
    public void finishById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishById(id);
        if (task == null)
            throw new TaskNotFoundException();
    }

    @Override
    public void finishByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.finishByIndex(index);
        if (task == null)
            throw new TaskNotFoundException();
    }

    @Override
    public void finishByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishByName(name);
        if (task == null)
            throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println((Arrays.toString(Status.values())));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusById(id, status);
        if (task == null)
            throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS]");
        System.out.println((Arrays.toString(Status.values())));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusByIndex(index, status);
        if (task == null)
            throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println((Arrays.toString(Status.values())));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusByName(name, status);
        if (task == null)
            throw new TaskNotFoundException();
    }

}
