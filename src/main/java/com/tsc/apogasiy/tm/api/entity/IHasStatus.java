package main.java.com.tsc.apogasiy.tm.api.entity;

import main.java.com.tsc.apogasiy.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
