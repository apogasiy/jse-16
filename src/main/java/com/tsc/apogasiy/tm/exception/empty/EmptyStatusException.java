package main.java.com.tsc.apogasiy.tm.exception.empty;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;

public class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error! Status is empty!");
    }

}
