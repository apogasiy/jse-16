package main.java.com.tsc.apogasiy.tm.exception.system;

import main.java.com.tsc.apogasiy.tm.constant.TerminalConst;
import main.java.com.tsc.apogasiy.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(final String command) {
        super("Error! Command '" + command + "' was not found! Use command '" + TerminalConst.HELP + "' to display command list");
    }

}
